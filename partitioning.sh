#! /bin/bash

# How the partition scheme will be:
# DEVICE          disk  
# ├─DEVICE_1      part  /boot
# └─DEVICE_2      part  
#   └─DEVICE_LVM    
#     ├─nixos-swap lvm   [SWAP]
#     └─nixos-root lvm   /

#############
#           #
# VARIABLES #
#           #
#############

BOOT=$1
DEVICE=$2
LUKS=$5
LVM=$6

#############

#############
#           #
# FUNCTIONS #
#           #
#############

function luks {

        cryptsetup luksFormat --type luks2 ${DEVICE_2} # crypting the partition
        cryptsetup luksOpen ${DEVICE_2} crypt       # opening it to work on it
        
        DEVICE_2=/dev/mapper/crypt
}

function lvm {
        
        parted ${DEVICE} -- mkpart primary 1GiB 100% # what will be the lvm partition
        
        if [[ ${LUKS} ]] then
                luks
        fi

        pvcreate ${DEVICE_2}       # physical volume
        vgcreate nixos ${DEVICE_2} # volume group

        lvcreate -L 2GiB nixos -n swap        # logical volume for swap
        lvcreate -L 5GiB nixos -n deleteLater # snapshot space
        lvcreate -L 100%FREE nixos -n root    # logical volume for nixos root
        lvremove /dev/nixos/deleteLater

        DEVICE_ROOT=/dev/nixos/root
        DEVICE_SWAP=/dev/nixos/swap
}

#############

##########
#        #
# SCRIPT #
#        #
##########

## PREPARING DEVICES

if [[ ${DEVICE} = "/dev/sda" ]]; then
        DEVICE_1=${DEVICE}1
        DEVICE_2=${DEVICE}2
        DEVICE_3=${DEVICE}3
elif [[ ${DEVICE} = "/dev/nvme0n1" ]]; then
        DEVICE_1=${DEVICE}p1
        DEVICE_2=${DEVICE}p2
        DEVICE_3=${DEVICE}p3
fi

###

## PARTITIONING

if [[ ${BOOT} = uefi ]]; then
        parted ${DEVICE} -- mklabel gpt 	       # GPT/UEFI
        parted ${DEVICE} -- mkpart ESP fat32 1MiB 1GiB # 1GiB boot partition
elif [[ ${BOOT} = bios ]]; then
        parted ${DEVICE} -- mklabel msdos              # MBR/BIOS
        parted ${DEVICE} -- mkpart primary 1MiB 1GiB   # 1GiB boot partition
else
        echo "Not uefi or bios, device not supported"
        exit 1
fi

parted ${DEVICE} -- set 1 boot on            # boot flag 

###

## FORMATTING

if [[ ${LVM} ]]; then
        lvm
else
        DEVICE_ROOT=${DEVICE_1}
        DEVICE_SWAP=${DEVICE_3}
fi

mkfs.fat -F 32 -n boot ${DEVICE_BOOT} # boot partition
mkfs.xfs -L nixos-root ${DEVICE_ROOT} # root partition
mkswap -L swap ${DEVICE_SWAP}         # swap partition

# swap turned on
swapon /dev/disk/by-label/swap

###

## MOUNTING 

mount /dev/disk/by-label/nixos-root /mnt
mkdir -p /mnt/boot
mount /dev/disk/by-label/boot /mnt/boot

###
