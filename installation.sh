#! /bin/bash

#############
#           #
# VARIABLES #
#           #
#############

INVALID_OPTION_MESSAGE="${OPTION}: Invalid OPTION. Please insert a valid option."
CONFIRM_MESSAGE="You chose ${OPTINO}. Are you sure? [y/n]"

CONFIG_DIR=/mnt/etc/nixos/
CONFIG=${CONFIG_DIR}configuration.nix

CONFIG_HW_DIR=${CONFIG_DIR}hardware/
CONFIG_HW=${CONFIG_HW_DIR}hardware.nix
CONFIG_HW_MODEL=${CONFIG_DIR}hardware/hardwareModel.nix

CONFIG_SYS_DIR=${CONFIG_DIR}system/
CONFIG_SYS=${CONFIG_SYS_DIR}system.nix
CONFIG_SYS_MODEL=${CONFIG_DIR}system/systemModel.nix

CONFIG_USR_DIR=${CONFIG_DIR}users/
CONFIG_USR=${CONFIG_USR_DIR}users.nix
CONFIG_USR_MODEL=${CONFIG_USR_DIR}userModel.nix
CONFIG_USR_KEYS_DIR=${CONFIG_DIR}keys/

#############

#############
#           #
# FUNCTIONS #
#           #
#############

function partitioning {
        ### CHOOSING PARTITION SCHEME

        # TODO
        # PRINT ON SCREEN THE DISKS LIST AND LET THE USER CHOOSE WHICH ONE [S]HE WANTS

        echo "Do you want LVM? [y,n]"
        while [[ true ]]; do
        
                read OPTION
       
                if [[ ${OPTION} = 'y' ]]; then
                        LVM=0
                        break
                elif [[ ${OPTION} = 'n' ]]; then
                        LVM=1
                        break
                else 
                        echo ${INVALID_OPTION_MESSAGE}
                fi
        done          

        echo "Do you want LUKS for your /(root) partition? [y,n]"
        while [[ true ]]; do

                read OPTION
       
                if [[ ${OPTION} = 'y' ]]; then
                        LUKS=0
                        break
                elif [[ ${OPTION} = 'n' ]]; then
                        LUKS=1
                        break
                else 
                        echo ${INVALID_OPTION_MESSAGE}
                fi
        done

        # Getting the boot options
        # 1. GPT/UEFI
        # 2. MBR/BIOS
        if [[ -d "/sys/firmware/efi" ]]; then
                BOOT="uefi"
        else
                BOOT="bios"
        fi
        ###

        # NixOS will always defaults to the first disk it will find (nvme > sata) for now
        if [[ -b "/dev/nvme0n1" ]]; then
                DEVICE=/dev/nvme0n1
        elif [[ -b "/dev/sda" ]]; then
                DEVICE=/dev/sda
        else 
                echo "I can't find a proper driver where to install NixOS"
                exit 1
        fi
        ###

        # Calling the partition function
        sh ./partitioning.sh ${BOOT} ${DEVICE} ${LUKS} ${LVM}

        echo "I finished the partitioning!"
        ###
}

function hardware_automatic {

        local MW_HARDWARE=""
        
        echo "Choose one of the options:"
        echo "1. ThinkPad A485"
        echo "2. Laptop"
        echo "3. Desktop"

        while [[ true ]]; do

                CPU=false
                read OPTION

                case $OPTION in
                        1) 
                                HW_HARDWARE="./ThinkPad_A485.nix"
                                break
                                ;;
                        2) 
                                HW_HARDWARE="./laptop.nix"
                                CPU=true
                                break
                                ;;
                        3) 
                                HW_HARDWARE="./common.nix"
                                CPU=true
                                break
                                ;;
                        *) 
                                echo ${INVALID_OPTION_MESSAGE}
                                ;;
                esac
        done

        if [[ ${CPU} ]]; then
                if [[ $(lscpu | grep AMD) ]]; then
                        HW_HARDWARE=${HW_HARDWARE},"\n    ./cpu/amd.nix"
                elif [[ $(lscpu | grep Intel) ]]; then
                        HW_HARDWARE=${HW_HARDWARE},"\n    ./cpu/intel.nix"
                else
                        echo "I didn't find a supported CPU model"
                        exit 1
                fi
        fi

        echo "{" >> ${CONFIG_HW}
        echo "  imports = [" >> ${CONFIG_HW}
        echo "    ${HW_HARDWARE}" >> ${CONFIG_HW}
        echo "  ];" >> ${CONFIG_HW}
        echo "}" >> ${CONFIG_HW}
}

function hardware_manual {
        cp ${CONFIG_HW_MODEL} ${CONFIG_HW}
        echo "Now you'll be able to modify the hardware configuration file. Follow what's written inside.\n"
        sleep 5s
        nano ${CONFIG_HW}
}

function system_automatic {

        SYS_MODULEARGS="hostName = \"localhost\";"

        if [[ ${LUKS} ]]; then
                SYS_MODULEARGS=${SYS_MODULEARGS},"\n    luksDevice = \"${LUKSDEVICE}\";"
        fi

        SYS_MODULEARGS=${SYS_MODULEARGS},"\n    systemVersion = \"19.09\";"


        SYS_SYSTEM="./sysconfig.nix"
        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./boot/kernelLatest.nix"

        if [[ ${LUKS} ]]; then 
                SYS_SYSTEM=${SYS_SYSTEM},"\n    ./boot/luks.nix"
        fi

        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./de/gnome_full.nix"
        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./keymaps/it.nix"
        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./networking/networkManager.nix"
        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./services/cups.nix"
        SYS_SYSTEM=${SYS_SYSTEM},"\n    ./services/resolved.nix"

        echo "{" >> ${CONFIG_SYS}
        echo "  _module.args = {" >> ${CONFIG_SYS}
        echo "    ${SYS_MODULEARGS}" >> ${CONFIG_SYS}
        echo "  };" >> ${CONFIG_SYS}
        echo "" >> ${CONFIG_SYS}
        echo "  imports = [" >> ${CONFIG_SYS}
        echo "    ${SYS_SYSTEM}" >> ${CONFIG_SYS}
        echo "  ];" >> ${CONFIG_SYS}
        echo "}" >> ${CONFIG_SYS}
}

function hardware_manual {
        cp ${CONFIG_SYS_MODEL} ${CONFIG_SYS}
        echo "Now you'll be able to modify the system configuration file. Follow what's written inside.\n"
        sleep 5s
        nano ${CONFIG_SYS}
}

function user_automatic {
        local PROGRAMS=""
        PROGRAMS="      ./programs/common.nix"

        echo "{ pkgs, ... }:" >> ${CONFIG_USR_USERNAME}
        echo "" >> ${CONFIG_USR_USERNAME}
        echo "{" >> ${CONFIG_USR_USERNAME}
        echo "" >> ${CONFIG_USR_USERNAME}
        echo "  users.users.${USERNAME} = {" >> ${CONFIG_USR_USERNAME}
        echo "    extraGroups = [ \"audio\" \"networkmanager\" \"networking\" \"wheel\" ];" >> ${CONFIG_USR_USERNAME}
        echo "    passwordFile = ${CONFIG_USR_KEYS_DIR}users-${USERNAME}\";" >> ${CONFIG_USR_USERNAME}
        echo "    isNormalUser = true;" >> ${CONFIG_USR_USERNAME}
        echo "  };" >> ${CONFIG_USR_USERNAME}
        echo "" >> ${CONFIG_USR_USERNAME}
        echo "  home-manager.users.${USERNAME} = { pkgs, ... }: {" >> ${CONFIG_USR_USERNAME}
        echo "    programs.home-amanger.enable = true;" >> ${CONFIG_USR_USERNAME}
        echo "    nixpkgs.config.allowUnfree = true;\n" >> ${CONFIG_USR_USERNAME}
        echo "    imports = [\n" >> ${CONFIG_USR_USERNAME} 
        echo ${PROGRAMS} >> ${CONFIG_USR_USERNAME}
        echo "    ];\n  };\n}" >> ${CONFIG_USR_USERNAME}
}

function user_manual {
        cp ${CONFIG_USR_MODEL} ${CONFIG_USR}
        echo "Now you'll be able to modify the user configuration file. Follow what's written inside.\n"
        sleep 5s
        nano ${CONFIG_USR}
}

#############

### PARTITIONING ##############################################################
###                                                                         ###
### ##### ##### ##### ##### ##### ##### ##### ##### #   # ##### #   # ##### ###
### #   # #   # #   #   #     #     #     #   #   # ##  #   #   ##  # #     ###
### ##### ##### #####   #     #     #     #   #   # # # #   #   # # # #  ## ###
### #     #   # #  #    #     #     #     #   #   # #  ##   #   #  ## #   # ###
### #     #   # #   #   #   #####   #   ##### ##### #   # ##### #   # ##### ###
###                                                                         ###
### PARTITIONING ##############################################################

echo "Do you want to wipe your drive and install NixOS on it?"

while [[ true ]]; do

        read -p "[y/n]: " OPTION

        if [[ ${OPTION} = "y" ]]; then 
                partitioning
                break
        elif [[ ${OPTION} = "n" ]]; then
                echo "Do you already mounted the right partitions?"
                read -p "[y/n]: " OPTION
                if [[ ${OPTION} = "n" ]]; then
                        echo "Do it"
                        exit 1
                fi
                break
        else
                echo "Write [y]es or [n]o.\n"
        fi
done

### CONFIGURING #########################################################
###                                                                   ###
### ##### ##### #   # ##### ##### ##### #   # ##### ##### #   # ##### ###
### #     #   # ##  # #       #   #     #   # #   #   #   ##  # #     ###
### #     #   # # # # #####   #   #  ## #   # #####   #   # # # #  ## ###
### #     #   # #  ## #       #   #   # #   # #  #    #   #  ## #   # ###
### ##### ##### #   # #     ##### ##### ##### #   # ##### #   # ##### ###
###                                                                   ###
### CONFIGURING #########################################################

### GENERATING EXPECTED FILES

nixos-generate-config --root /mnt
nix-env -iA nixos.git
git clone https://gitlab.com/DunkleCat/nixos-config
mv -f nixos-config/* /mnt/etc/nixos/

##

#######################
##                   ##
## CONFIGURATION.NIX ##
##                   ##
#######################

echo "Writing configuration.nix..."

echo "# Help is available in the configuration.nix(5) man page" >> ${CONFIG}
echo "# and in the NixOS manual (accessible by running `nixos-help`)\n" >> ${CONFIG}
echo "{ config, pkgs, ... }:\n\n{\n" >> ${CONFIG}
echo "" >> ${CONFIG}
echo "  imports = [" >> ${CONFIG}
echo "" >> ${CONFIG}
echo "    ./hardware-configuration.nix" >> ${CONFIG}
echo "    ${CONFIG_HW}" >> ${CONFIG}
echo "    ${CONFIG_SYS}" >> ${CONFIG}
echo "    ${CONFIG_USR}" >> ${CONFIG}
echo "" >> ${CONFIG}
echo "    \"${ builtins.fetchTarball https://github.com/rycee/home-manager/archive/master.tar.gz }/nixos\"" >> ${CONFIG}
echo "  ];" >> ${CONFIG}
echo "}" >> ${CONFIG}

echo "Configuration.nix written!"

################################################################################

##################
##              ##
## HARDWARE.NIX ##
##              ##
##################

echo "Writing hardware.nix..."

echo "You can let the script create a configuration file for your hardware, or manually edit it."
echo "1. Automatic"
echo "2. Manual"
echo ""

while [[ true ]]; do
        read -p "How do you want to proceed? [1,2]: " OPTION

        case $OPTION in
                1) 
                        hardware_automatic
                        break
                        ;;
                2)
                        hardware_manual
                        break
                        ;;
                *)
                        echo "Option invalid"
                        ;;
        esac
done

echo "hardware.nix written!"

###############################################################################

################
##            ##
## SYSTEM.NIX ##
##            ##
################

echo "Writing system.nix..."

echo "You can let the script create a configuration file for your system, or manually edit it."
echo "1. Automatic"
echo "2. Manual"
echo ""

while [[ true ]]; do
        read -p "How do you want to proceed? [1,2]: " OPTION

        case $OPTION in
                1) 
                        system_automatic
                        break
                        ;;
                2)
                        system_manual
                        break
                        ;;
                *)
                        echo "Option invalid"
                        ;;
        esac
done

echo "system.nix written!"

###############################################################################

###############
##           ##
## USERS.NIX ##
##           ##
###############

echo "Writing users.nix..."

while [[ true ]]; do

        read -p "Write the username you would like to have: " USERNAME

        read -p "\nYou wrote ${USERNAME}. Is it correct? [y/n]: " OPTION

        if [[ ${OPTION} = "y" ]]; then
                break
        fi
done

while [[ true ]]; do

        read -sp "Write your password: " PASSWORD
        read -sp "Confirm your password: " PASSWORD_CONFIRMATION

        if [[ ${PASSWORD} = ${PASSWORD_CONFIRMATION} ]]; then 
                echo "Password setted"
                mkpasswd ${PASSWORD} -m sha-512 > ${CONFIG_DIR}keys/users-${USERNAME}
                break
        else 
                echo "Passwords are not equals. Rewrite them."
        fi
done

CONFIG_USR_USERNAME=${CONFIG_USR_DIR}${USERNAME}.nix
touch ${CONFIG_USR_USERNAME}

echo "You can let the script create a basic configuration file for your user, or manually edit it."
echo "1. Automatic"
echo "2. Manual"
echo ""

while [[ true ]]; do
        read -p "How do you want to proceed? [1,2]: " OPTION

        case $OPTION in
                1) 
                        user_automatic
                        break
                        ;;
                2)
                        user_manual
                        break
                        ;;
                *)
                        echo "Option invalid"
                        ;;
        esac
done

echo "{" >> ${CONFIG_USR}
echo "  imports = [" >> ${CONFIG_USR}
echo "    ./${USERNAME}.nix" >> ${CONFIG_USR}
echo "    ./guest.nix" >> ${CONFIG_USR}
echo "  ];" >> ${CONFIG_USR}
echo "}" >> ${CONFIG_USR}

echo "users.nix written!"

###############################################################################

echo "\n\nNow run nixos-install to start the creation of your system"
# nixos-install
