NIXOS INSTALL INTERFACE
=======================

This project will be divided in two pieces:
* `nixos-install_script`     a bunch of shell scripts that will write the config file  
* `nixos-install_interfaces` various interfaces implemented in Haskell

# nixos-install\_cript

A simple collection of scripts for creating nixos' config using my [configuration structure](https://gitlab.com/DunkleCat/nixos-config)

# nixos-install\_interfaces

* CLI [link](https://hackage.haskell.org/package/cli)
* TUI [link](https://hackage.haskell.org/package/ncurses)
* GUI [link](https://hackage.haskell.org/package/gtk3)
